# Проект Assist4Edu: Применение больших языковых моделей для образования

## Введение

В современном мире образование постоянно эволюционирует благодаря прогрессу в области информационных технологий. Проект Assist4Edu нацелен на исследование и внедрение передовых методов Data Science и искусственного интеллекта, особенно больших языковых моделей, для улучшения и оптимизации образовательного процесса. 

## Цель проекта

Основная цель проекта - изучить потенциал больших языковых моделей в образовательной среде и разработать инструменты, которые могли бы значительно повысить эффективность обучения и дать новые возможности как учащимся, так и преподавателям.

## Методология проекта

### Agile и CRISP-ML(Q)

Проект будет вестись с применением гибкой методологии Agile, обеспечивающей высокую адаптивность и оперативность внесения изменений и улучшений. В качестве основы для исследований и разработок в области машинного обучения будет использоваться методология CRISP-ML(Q), адаптированная специально под задачи машинного обучения, что позволит систематизировать процесс исследования от постановки задач до оценки результатов.

### Процесс исследования

Исследовательский процесс включает в себя следующие ключевые этапы:

- **Сбор данных**: направлен на поиск и сбор релевантных данных для анализа и обучения моделей.
- **Постановка гипотез**: формулирование предположений на основе имеющихся данных и предварительного анализа.
- **Применение библиотек и инструментов**: использование современных библиотек и инструментов для обработки данных, обучения моделей и анализа результатов.
- **Проведение экспериментов**: тестирование гипотез и моделей на практике с целью проверки их эффективности и адекватности.
- **Тестирование**: оценка точности и надежности полученных результатов, их проверка на новых данных.

### RAG система для QA

Для обеспечения качества результатов исследований будет использоваться RAG (Retrieval-Augmented Generation) система для вопросов и ответов (QA), основанная на использовании документов занятий курсов образовательной платформы. Это позволит создать высокоэффективную систему QA, интегрированную в образовательный процесс и способствующую улучшению понимания учебного материала.

## Присоединяйтесь к нам

Мы приглашаем исследователей, разработчиков, преподавателей и всех, кого интересует применение инноваций в образовании, принять участие в проекте Assist4Edu. Ваш вклад и сотрудничество помогут создать новые инструменты обучения и сделать образовательный процесс более эффективным и доступным.

# Проект на Python с использованием Poetry и Jupyter Notebook

Это инструкции по развертыванию и запуску Python-проекта, использующего систему управления зависимостями Poetry и Jupyter Notebook внутри Docker контейнера.

## Перед началом

Убедитесь, что на вашей машине установлены Docker и, если нужно, Docker Compose. Инструкции по установке Docker можно найти на официальном сайте https://docs.docker.com/get-docker/.

## Сборка Docker образа

Для сборки Docker образа выполните следующую команду из корневой директории проекта, где находится Dockerfile:

```sh
docker build -t имя_проекта .
```

Замените `имя_проекта` на желаемое имя для вашего Docker образа.

## Запуск контейнера

Чтобы запустить контейнер из собранного образа, используйте команду:

```sh
docker run -p 8888:8888 имя_проекта
```

Эта команда запустит Jupyter Notebook на порту 8888, и вы сможете открыть его в браузере по адресу http://localhost:8888.

## Работа с Jupyter Notebook

После запуска контейнера Jupyter Notebook будет доступен по адресу http://localhost:8888. Открыв этот адрес в браузере, вы попадете в рабочую среду Jupyter, где сможете создавать и редактировать ноутбуки.

## Завершение работы

Чтобы остановить и удалить контейнер, используйте следующие команды Docker:

```sh
docker stop <container_id>
docker rm <container_id>
```

Где `<container_id>` - это идентификатор вашего запущенного контейнера, который можно найти с помощью команды `docker ps`.

Спасибо за использование данного проекта!


Спасибо за ваш интерес и желание внести вклад в образование будущего!