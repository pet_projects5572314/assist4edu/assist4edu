# Используем официальный базовый образ Python
FROM python:3.11.5-slim

# Установка базовых переменных окружения для python
ENV PYTHONFAULTHANDLER=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONHASHSEED=random \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100

# Устанавливаем дополнительные системные пакеты и инструменты для установки Poetry
RUN apt-get update \
    && apt-get install -y --no-install-recommends curl git \
    && rm -rf /var/lib/apt/lists/*

# Установка и настройка Poetry
ENV POETRY_VERSION=1.7.1 \
    POETRY_HOME="/opt/poetry" \
    POETRY_VIRTUALENVS_CREATE=true \
    POETRY_NO_INTERACTION=1 
RUN curl -sSL https://install.python-poetry.org | python3 - \
    && ln -s $POETRY_HOME/bin/poetry /usr/local/bin/poetry

# Копирование файлов настройки зависимостей
WORKDIR /app
COPY pyproject.toml /app/

# Следуя best practices, устанавливаем зависимости перед копированием всего проекта
RUN poetry config virtualenvs.create true \
    && poetry install $(test "$YOUR_ENV" == production && echo "--no-dev") --no-interaction --no-ansi

# Копируем оставшиеся файлы проекта
COPY . /app

# Открываем порт для Jupyter Notebook
EXPOSE 8888

# Команда запускает Jupyter Notebook через Poetry
CMD ["poetry", "run", "jupyter", "notebook", "--ip=0.0.0.0","--port=8888", "--allow-root", "--no-browser"]